// No. 1 Looping While

var i = 2;
console.log("LOOPING PERTAMA")
while(i <= 20){
    console.log(i + " - I love coding")
    i+=2
}
var i = 20;
console.log("LOOPING KEDUA")
while(i >= 2){
    console.log(i + " - I will become a mobile developer")
    i-=2
}


// No. 2 Looping menggunakan for

var i = 1
for(var i = 1; i <= 20; i++){
    if(i%2 == 0){
        console.log(i + " - Berkualitas");
    }
    else if(i%3 == 0 && i%2 == 1){
        console.log(i + " - I Love Coding");
    }
    else {
        console.log(i + " - Santai");
    }
}


// No. 3 Membuat Persegi Panjang #

var persegi = "####";
for(const char of persegi){
    console.log(char.repeat(8));
}


//Soal 4 Membuat Tangga #

var i = 7;
var ii ='';

for(var k = 0; k < i; k++){
    for(var l = 0; l <= k; l++){
        ii += '#'
    }ii += '\n'
}console.log(ii)


//Soal 5 Membuat Papan Catur

for(i = 1; i <= 8; i++){
    if((i%2) === 1){
        console.log(' # # # #')
    }else
    console.log('# # # #')
}
