// TEST CASES Ascending Ten

function AscendingTen(num) {
    let hasil1 = ""
    if (!num) {
      return -1
    } else {
      for (let i = num; i < num + 10; i++) {
        hasil1 += `${String(i)} `
      }
    }
    return hasil1
}
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())

// TEST CASES Descending Ten

function DescendingTen(num) {
    let hasil2 = ""
    if (!num) {
      return -1
    } else {
      for (let j = num; j > num - 10; j--) {
        hasil2 += `${String(j)} `
      }
    }
    return hasil2
  
} 
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())

// TEST CASES Conditional Ascending Descending

function ConditionalAscDesc(reference, check) {
    if (!reference || !check) {
      return -1
    } else {
      if (check % 2 == 0) {
        return DescendingTen(reference)
      } else if (check % 2 !== 0) {
        return AscendingTen(reference)
      }
    }
}
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc())

// TEST CASE Ular Tangga

function ularTangga() {
    let hasil4 = ""
    let counter = 10
    for (let k = 100; k > 0; k--) {
      if (counter % 2 == 0) {
        hasil4 += ConditionalAscDesc(k, counter)
        hasil4 += "\n"
        counter++
        k -= 18
      } else {
        hasil4 += ConditionalAscDesc(k, counter)
        hasil4 += "\n"
        counter++
      }
    }
    return hasil4
}
console.log(ularTangga())