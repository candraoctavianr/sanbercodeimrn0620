// Tugas 6 – Object Literal
// Soal No. 1 (Array to Object)

function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear();

  var objTemp = {};
  var arrayOfObj = [];

  for (var x = 0; x < arr.length; x++) {
    objTemp.firstName = arr[x][0];
    objTemp.lastName = arr[x][1];
    objTemp.gender = arr[x][2];
    if (arr[x][3] == null) {
      objTemp.age = "Invalid birth year";
    } 
    else {
      if (arr[x][3] > thisYear) {
        objTemp.age = "Invalid birth year";
      } 
      else {
        objTemp.age = thisYear - arr[x][3];
      }
    }

    arrayOfObj.push(objTemp);
    objTemp = {}; 
  }
  if (arrayOfObj.length == 0) {
    console.log('""');
  } 
  else {
    for (var x = 0; x < arrayOfObj.length; x++) {
      console.log(
        x +
          1 +
          ". " +
          arrayOfObj[x].firstName +
          " " +
          arrayOfObj[x].lastName +
          ": " +
          JSON.stringify(arrayOfObj[x])
      );
    }
  }
}

var people = [
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
arrayToObject([]);

// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
  var storeX = {
    "Sepatu Stacattu": 1500000,
    "Baju Zoro": 500000,
    "Baju H&N": 250000,
    "Sweater Uniklooh": 175000,
    "Casing Handphone": 50000,
  };
  if (memberId == null || memberId === "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } 
  else {
    if (money < 50000) {
      return "Mohon maaf, uang tidak cukup";
    } 
    else {
      var prices = Object.values(storeX).sort();
      var listPurchased = [];
      var total = 0;
      for (var x = 0; x < prices.length; x++) {
        if (prices[x] < money) {
          total += prices[x];
          listPurchased.push(
            Object.keys(storeX)[Object.values(storeX).indexOf(prices[x])]
          );
        }
      }
      var result = {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: money - total,
      }
      return result;
    }
  }
}
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000)); 
console.log(shoppingTime("234JdhweRxa53", 15000)); 
console.log(shoppingTime());

// Soal No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  var result = [];
  if (arrPenumpang != null) {
    for (var x = 0; x < arrPenumpang.length; x++) {
      var ruteUser = rute.slice(
        rute.indexOf(arrPenumpang[x][1]),
        rute.indexOf(arrPenumpang[x][2])
      );
      var bayar = 2000 * ruteUser.length;
      var data = {
        penumpang: arrPenumpang[x][0],
        naikDari: arrPenumpang[x][1],
        tujuan: arrPenumpang[x][2],
        bayar: bayar,
      };
      result.push(data);
      data = {};
    }
    return result;
  } 
  else {
    return arrPenumpang;
  }
}
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
console.log(naikAngkot([]));