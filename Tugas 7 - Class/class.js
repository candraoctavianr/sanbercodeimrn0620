// Tugas 7 – Class
// 1. Animal Class 

class Animal {
    constructor(name) {
      this.name = name;
    }
    get legs() {
      return 4;
    }
    get cold_blooded() {
      return false;
    }
  }
  var sheep = new Animal("shaun");
  
  console.log(sheep.name);
  console.log(sheep.legs);
  console.log(sheep.cold_blooded);

  class Ape extends Animal {
    constructor(name) {
      super(name);
      this.name = name;
    }
    get legs() {
      return 2;
    }
    yell() {
      console.log("Auooo");
    }
  }
  class Frog extends Animal {
    constructor(name) {
      super(name);
      this.name = name;
    }
    jump() {
      console.log("hop hop");
    }
  }
  var sungokong = new Ape("kera sakti");
  sungokong.yell();
  var kodok = new Frog("buduk");
  kodok.jump();

// 2. Function to Class

class Clock {
    constructor({ template }) {
        this.template = template;
    }
    render = () => {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = "0" + hours;
        var mins = date.getMinutes();
        if (mins < 10) mins = "0" + mins;
        var secs = date.getSeconds();
        if (secs < 10) secs = "0" + secs;
        
        var output = this.template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);
        console.log(output);
    }
    stop = () => {
        clearInterval(this.timer)
    }
    start = () => {
        this.render()
        this.timer = setInterval(this.render, 1000)
    }
}
var clock = new Clock({template: "h:m:s"});
clock.start();