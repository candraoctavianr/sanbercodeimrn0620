import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Tugas12 from './Tugas/Tugas12/App'
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import RegisterScreen from './Tugas/Tugas13/RegisterScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import Tugas15 from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Tugas/Quiz3/index'
import Tugas16 from './Tugas/Tugas16/index'

export default function App() {
  return (
    // <View style={styles.container}>
    // </View>
    // <Tugas12 />
    // <LoginScreen />
    // <RegisterScreen/>
    // <AboutScreen />
    // <SkillScreen />
    // <Tugas15 />
    // <TugasNavigation />
    // <Quiz3 />
    <Tugas16 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
